# -*- encoding : utf-8 -*-
class PageDiffsController < ApplicationController
	include ApplicationHelper
	def showdiff
		if Settings.kiosk_mode
			render_not_found
			return
		end
		@diff = PageDiff.new(params[:sha], params[:pagename].to_s)
	 
		if !@diff.exists?
			render_not_found
			return
		end	
		@sha = params[:sha]
		@page = nil
		render
	end
	
	def showpagediff
		if Settings.kiosk_mode
			render_not_found
			return
		end
		@diff = PageDiff.new(params[:sha])
		@sha = params[:sha]
		@page = WikiPage.new params[:pagename]
		if !@page.exists? or !@diff.exists?
			render_not_found
			return
		end
		render
	end
end
