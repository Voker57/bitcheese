class ApplicationController < ActionController::Base
	def setcss
		@custom_csses = Dir["#{Rails.root}/public/stylesheets/custom-css/*"].map{|a| a.split("/").last}
		if params[:css]
			if params[:css] == "reset"
				session.delete :custom_css
			else
				session[:custom_css] = params[:css]
			end
			redirect_to({})
		else
			render
		end
	end
end
