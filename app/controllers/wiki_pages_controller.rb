class WikiPagesController < ApplicationController
	include ApplicationHelper
	
	before_action :add_current_page_rss, only: [:page, :src, :log]
	def add_current_page_rss
		@feeds ||= []
		@feeds << {:title => "Recent changes to #{params[:pagename]}", :link => url_for(controller: :wiki_pages, action: :log, format: :atom, pagename: params[:pagename])}
	end
	
	def page
		@page = WikiPage.new(params[:pagename])
		if !@page.exists?
			render_not_found
			return
		end
		
		if @page.link?
			redirect_to("/" + resolve(@page.contents, request.path_info))
		elsif @page.tree? and not request.original_url =~%r|/$|
			redirect_to(request.path_info + "/")
		else
			if @page.erb? and Settings.erb
				@marked_up_content = render_to_string(inline: @page.unmarked_content)
			else
				@marked_up_content = @page.marked_up_content
			end
			render
		end
	end
	
	def src
		@page = WikiPage.new(params[:pagename])
		render
	end
	
	def log
		if Settings.kiosk_mode
			render_not_found
			return
		end
		@page=WikiPage.new(params[:pagename])
		if @page.exists? 
			respond_to do |format|
				format.html
				format.atom
			end
		else
			render_not_found
		end
	end
end
