# -*- encoding : utf-8 -*-
class WikiPage
	include ApplicationHelper
	attr_reader :name, :desc_map
	 
	@@git = Rugged::Repository.new(Settings.git_dir, :is_bare => true)
	def initialize(pname) 
		pname.gsub!(/^\/+|\/+$/,'')
		@name = pname
		@name.force_encoding("UTF-8")
		if pname.empty?
			@object = @@git.head.target.tree
			@fileinfo = nil
		else
			begin
				@fileinfo = @@git.head.target.tree.path(@name)
				@object = @@git.lookup(@fileinfo[:oid])
			rescue Rugged::TreeError
				@object = nil
			end
			
		end
	end

	def title
		markup! if !@title
		t = if @title.blank?
			name.blank? ? "/" : name
		else
			@title
		end
		t.force_encoding("UTF-8")
		# TODO: fix this shit on rugged level?
		t
	end

	def unmarked_content
		markup! if !@unmarked_content
		@unmarked_content
	end
	
	def marked_up_content
		markup! if !@marked_up_content
		@marked_up_content
	end

	def erb?
		@name =~ /\.erb$/
	end

	def markup!
		@unmarked_content = if tree?
			index ? index.content : ""
		else
			@object.content
		end

		index_content = nil
		if @unmarked_content.split("\n").first =~ /\s+-\s+/
			desc_map_content, index_content = @unmarked_content.split("\n\n").first, @unmarked_content.gsub(/\A.*?\n\n/m,"")
			if desc_map_content == index_content
				index_content = ""
			end
		else
			index_content = @unmarked_content
		end
		if not erb? or not Settings.erb 
			begin
				Timeout.timeout(10) do
					Open3.popen3("polebrush -escape-html -print-header") do |stdin, stdout, stderr|
						stdin.write(index_content.force_encoding("UTF-8"))
						stdin.close
						err = stderr.read.force_encoding("UTF-8")
						Rails.logger.error "polebrush fucked up: #{err}" unless err.empty?
						result = stdout.read.force_encoding("UTF-8")
						title = result.scan(/\AHeader: (.*?)$/m)[0][0]
						markedup = result.gsub(/\A.*?$/m, "")
						@marked_up_content = markedup
						@title = title
					end
				end
			rescue Timeout::Error
				throw "Polebrush fuckup"
			end
		end
		@desc_map = {}
		if desc_map_content
			desc_map_content.each_line do |l|
				a = l.chomp.split(/\s+-\s+/)
				@desc_map[a[0]] = a[1..-1].join(" - ") if a.size > 1
			end
		end
	end
	
	def lastedit
		begin
			cmt = commits.first
			"#{cmt.author[:name]} on #{cmt.author[:time]}"
		rescue
			"never"
		end
	end

	def lastedit_date
		begin
			commits.first.author[:time]
		rescue
			"0"
		end
	end

	def commits
		@commits ||= @@git.log_at(@@git.head.target, @name).to_a
	end

	def index
		if @object.kind_of? Rugged::Tree and @object['index']
			@@git.lookup(@object['index'][:oid])
		else
			nil
		end
	end

	def self.diff(sha)
		@@git.lookup(sha).diff.patch
	end

	def article?
		@object.kind_of? Rugged::Blob and !link?
	end

	def tree?
		@object.kind_of? Rugged::Tree
	end

	def link?
		WikiPage.is_link? @name
	end

	def contents
		if tree?
			@object.content
		else
			@object.content.force_encoding("UTF-8")
		end
	end

	def trees
		if tree?
			a = []
			@object.each_tree do |entry|
				a << entry unless (entry[:filemode].to_i & (2 ** 13)) != 0
			end
			return a
		else
			nil
		end
	end

	def articles
		if tree?
			a = []
			@object.each_blob do |entry|
				a << entry unless (entry[:filemode].to_i & (2 ** 13)) != 0
			end
			return a
		else
			nil
		end
	end

	def exists?
		@object != nil
	end
	
	def self.is_link?(page_name)
		not page_name.blank? and (@@git.head.target.tree.path(page_name)[:filemode].to_i & (2 ** 13)) != 0
	end
end
