class PageDiff
	@@git = Rugged::Repository.new(Settings.git_dir, :is_bare => true)
	def initialize(ref,page='')
		@commit = @@git.log_at(@@git.lookup(ref),page).first
	end
	def patches
		@commit ? @commit.parents.first.diff(@commit).patches : []
	end
	def message
		@commit ? @commit.message : ''
	end
	def sha
		@commit ? @commit.oid : ''
	end
	def author
		@commit ? @commit.author[:name] : ''
	end
	def date
		@commit ? @commit.author[:time] : ''
	end
	def parents
		@commit ? @commit.parent_ids : []
	end
	def exists?
		!!@commit
	end
end
