module ApplicationHelper
	def simple_markup(str)
		str.gsub("\n", "<br />")
	end
	
	# Textile with my options
	def fullMarkup(str)
		
		Open3.popen3("polebrush -escape-html") do |stdin, stdout, stderr, w|
		begin
			Timeout.timeout(2) do
				stdin.write(str)
				stdin.close
				err = stderr.read
				Rails.logger.error "polebrush fucked up: #{err}" unless err.empty?
				stdout.read
			end
		rescue Timeout::Error
			Process.kill("KILL", w.pid)
			Rails.logger.error "polebrush fucked up"
			simple_markup(h(str))
		end
		end
	end

	def liteMarkup(str)
	
		Open3.popen3("polebrush -escape-html -escape-nomarkup -light") do |stdin, stdout, stderr, w|
		begin
			Timeout.timeout(2) do
				stdin.write(str.force_encoding("UTF-8"))
				stdin.close
				err = stderr.read
				Rails.logger.error "polebrush fucked up: #{err}" unless err.empty?
				stdout.read
			end
		rescue Timeout::Error
			Process.kill("KILL", w.pid)
			Rails.logger.error "polebrush fucked up"
			simple_markup(h(str))
		end
		end
	end

	def wikilink(name, loc='/')
		{controller: :wiki_pages, action: :page, pagename: resolve(name,loc)}
	end

	def wikilink_feed(name, loc='/')
		{controller: :wiki_pages, action: :log, pagename: resolve(name,loc), format: :xml}
	end

	def wikilink_src(name, loc='/')
		{controller: :wiki_pages, action: :src, pagename: resolve(name,loc)}
	end

	def wikilink_log(name, loc='/')
		{controller: :wiki_pages, action: :log, pagename: resolve(name,loc)}
	end

	def difflink(sha, pagename = nil)
		if(pagename)
			{controller: :page_diffs, action: :showpagediff, sha: sha, pagename: pagename}
		else
			{controller: :page_diffs, action: :showdiff, sha: sha}
		end
	end

	def full_wikilink(name)
		arr = name.split("/")
		linkstr = ["[#{link_to "root", wikilink("")}]"]
		curname = []
		arr.each do |v|
			curname << v unless v=='/'
			linkstr << link_to(v, wikilink(curname.join('/')))
		end
		linkstr.join('/')
	end

	def diff_markup(pstr, inline=false)
		str = begin
			Timeout.timeout(10) do
				Open3.popen3("wdiff -n -d -w \"<span style='color: #FF9EA0; background: #000D00;	text-decoration: line-through;'>\" -x '</span>' -y \"<span style='color: #8AFF63; background: #000D00;'>\" -z '</span>' ") do |stdin, stdout, stderr|
					stdin.write(h(pstr.force_encoding("UTF-8")))
					stdin.close
					out = stdout.read
					err = stderr.read
					Rails.logger.error "WDIFF fucked up: #{err}" unless err.empty?
					out
				end
			end
		
		rescue Timeout::Error
			throw "Wdiff fuckup"
		end
		
		if inline
			str.split("\n").map do |s|
				if s.empty?
					s
				else
					if s =~ /^@@/
						"<span style='color: #E200FF'>#{s}</span>"
					else
						s
					end
				end
			end.join("\n")
		else
			str.split("\n").map do |s|
				if s.empty?
					s
				else
					if s =~ /^@@/
						"<span class='diff-at'>#{s}</span>"
					else
						s
					end
				end
			end.join("\n")
		end
	end
	

	def resolve(str, loc='/')
		s = if str[0]=='/'
			str[1..-1]
		else
			loc = loc.split('/').slice(0..-2).join('/')
			loc = loc + '/' unless loc.empty? || loc[-1] == '/'
			glued = (loc + str).split('/')
			while glued.include?('..')
				ind = glued.index('..')
				if ind != 0
					glued.delete_at(ind - 1)
				end
				glued.delete_at(ind - 1)
			end
			glued.join('/')
		end
		s
	end

	def feeds2links(feeds)
		if feeds
			feeds.map do |f|
				s = "<link rel='alternate' type='application/atom+xml' title='#{h f[:title]}' href='#{h f[:link]}' />"
				s.force_encoding("UTF-8")
			end.join("\n").html_safe
		else
			nil
		end.to_s
	end

	alias_method :fM, :fullMarkup
	alias_method :lM, :liteMarkup
	
	def gravatar(email)
		#create the md5 hash
		hash = MD5::md5(email.downcase)

		# compile URL which can be used in <img src=""...
		"http://www.gravatar.com/avatar/#{hash}?s=80&r=x&d=identicon"
	end

	def custom_css
		if session[:custom_css]
			%|<link rel="stylesheet" href="/stylesheets/custom-css/#{h session[:custom_css]}" type="text/css" media="screen" charset="utf-8" />|.html_safe
		else
			"".html_safe
		end
	end

	def blobs_r(obj)
		if obj.is_a? Grit::Blob
			[obj]
		elsif obj.is_a? Grit::Tree
			obj.contents.map do |obj2|
				blobs_r obj2
			end.flatten
		else
			[]
		end
	end
	
	def render_not_found
		render "errors/not_found", status: 404
	end
end
