xml.instruct! :xml, :version => "1.0" 
xml.feed xmlns: "http://www.w3.org/2005/Atom" do
  xml.title "History for #{@page.name == "" ? "/" : h(@page.name)}"
  xml.subtitle "Recent changes to wiki page #{h(@page.title)}"
  xml.link href: url_for(wikilink(@page.name).merge(only_path: false))
  xml.link rel: "self", href: url_for(wikilink(@page.name).merge(only_path: false, format: :atom))
  xml.id url_for(wikilink(@page.name).merge(only_path: false, format: :atom))

  xml.updated @page.commits.first.author[:time].iso8601 if @page.commits.first
  
  @page.commits[0..20].each do |cmt|
  xml.entry do
    xml.title "#{cmt.oid[0..7]}: #{h(cmt.message.force_encoding("UTF-8").truncate(50))}"
    xml.link url_for(difflink(cmt.oid).merge(only_path: false))
    xml.id url_for(difflink(cmt.oid).merge(only_path: false))
    xml.updated cmt.author[:time].iso8601
    
    xml.content type: "html" do
      xml.cdata! simple_markup("<p><strong>#{h cmt.message.force_encoding("UTF-8")}</strong></p>"+(cmt.parents.size == 1 ?  diff_markup(cmt.parents.first.diff(cmt).patch.force_encoding("UTF-8"), true) : ""))
    end
    xml.author do
      xml.name h(cmt.author[:name])
    end
  end
  end
end
